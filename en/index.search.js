var relearn_search_index = [
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV \u003e BOOKS",
    "content": " The author’s first book offers readers a new system of knowledge and concepts of natural laws which are necessary for every thinking self to understand what is going on with him, with people around him and with our planet. This book is for those whose aim is to penetrate the secrets of nature, to understand the miracle of the origin of life, what a soul is and what happens with man during and after death. Such concepts as soul, spirit, reincarnation, etc. stop being mystic and incognizable and turn into natural concepts conditioned by the evolutional laws of living matter. For the first time, the explanation of almost all phenomena of living and lifeless nature and the unity of macro- and microcosmic laws is given in this book. The author has succeeded in creating a unified field theory and uniting the concepts of Nature into a single whole. The book contains 182 high-quality author’s illustrations.\n© Nicolai Levashov 1994, II Publishing 2000, San Francisco.\nDownload Download for free\n* Levashov-The-Final-Appeal-EN-1.zip (196 KБ)\n* Levashov-The-Final-Appeal-EN-2.zip (183 KБ)\n* The-Final-Appeal-Illustrations.zip (8,9 МБ) ",
    "description": "The author’s first book offers readers a new system of knowledge and concepts of natural laws which are necessary for every thinking self to understand what is going on with him, with people around him and with our planet. This book is for those whose aim is to penetrate the secrets of nature, to understand the miracle of the origin of life, what a soul is and what happens with man during and after death.",
    "tags": [
      "Levashov",
      "Books"
    ],
    "title": "\"The Final Appeal to Mankind\"",
    "uri": "/en/levashov/knigi/the-final-appeal/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV",
    "content": " The universe…the mystery of Life! How did life begin from non-living matter? How did it happen that inorganic atoms, combining in various orders and quan-tities, created organic molecules, which then evolved into living matter?\nThe enigma of life….How did it all begin, how did it unfold and how did it bring forth such a multiplicity of living forms?\nWhere did man come from when he first appeared on planet Earth? Did he de-velop slowly according to the Darwinian theory of evolution? Was he thrust from the Garden of Eden in order to expiate his sins before God? Or if not God’s doing, who brought man to this planet, when, and for what purpose? And how can we ac-count for the rich diversity of all the races of man?\nHow did we come to possess the capacity to think? What happens to us when we die and what awaits us after death?\nIf there is a soul — what is it? Where does it go when we die? If heaven and hell exist, then where are they? Why do we not see them, as have those who experi-ence clinical death and then return to life with reports of being welcomed by angels and drawn into a tunnel of light? What happens at the moment of conception and how does the human embryo develop?\nWhat is reincarnation? Do we live one or many times on this planet? Is it possi-ble for us to see into the past and future and to move through vast realms of time and space without a craft?\nAre we alone in the universe?",
    "description": "The universe…the mystery of Life! How did life begin from non-living matter? How did it happen that inorganic atoms, combining in various orders and quan-tities, created organic molecules, which then evolved into living matter?\nThe enigma of life….How did it all begin, how did it unfold and how did it bring forth such a multiplicity of living forms?\nWhere did man come from when he first appeared on planet Earth?",
    "tags": [],
    "title": "BOOKS",
    "uri": "/en/levashov/knigi/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK",
    "content": "Levashov N.V. Info The section contains materials that are the foundation for the latest “THEORY ABOUT EVERYTHING\". On the basis of this foundation, the materials of the site are built.\nThe author of the books is a researcher, russian scientist Nicolai Levashov.",
    "description": "Levashov N.V. Info The section contains materials that are the foundation for the latest “THEORY ABOUT EVERYTHING\". On the basis of this foundation, the materials of the site are built.\nThe author of the books is a researcher, russian scientist Nicolai Levashov.",
    "tags": [],
    "title": "LEVASHOV",
    "uri": "/en/levashov/index.html"
  },
  {
    "breadcrumb": "",
    "content": "Hello! We all live at a very interesting time when pseudo-scientific modern conceptions cannot conceal the ignorance hidden behind them. What ignorance, – someone may exclaim surprisingly, – it is impossible to talk about any kind of ignorance when our world is practically “overloaded” with the information of new discoveries, phenomena, hypotheses and theories and their practical realization.\nFirst of all, let us define, what the terms “information” and “knowledge” mean. A lot of people believe there is no difference between them. Apparent likeness of these two concepts may delude, but if we take a little time to think about these two words, as well as many other words used daily, we will be amazed. We forget that each word has its own distinct meaning as well as its own sound. Here is the meaning of the words: “information” and “knowledge”.\nInformation is a message about events happening around and inside us received through the organs of sense.\nKnowledge is a reflection and understanding of events happening around and inside us.\nAt first sight this distinction may seem insignificant. In fact, these two concepts differ remarkably from each other. Knowledge is information about events happening around and inside us, received through our sense organs, meditated on and finally comprehended. Unfortunately, our modern terrestrial civilization has accumulated a great deal of information about events happening around and inside us; however, understanding of this information is practically nonexistent.\nGo to the books..",
    "description": "Hello! We all live at a very interesting time when pseudo-scientific modern conceptions cannot conceal the ignorance hidden behind them. What ignorance, – someone may exclaim surprisingly, – it is impossible to talk about any kind of ignorance when our world is practically “overloaded” with the information of new discoveries, phenomena, hypotheses and theories and their practical realization.\nFirst of all, let us define, what the terms “information” and “knowledge” mean.",
    "tags": [],
    "title": "RAVESTNIK",
    "uri": "/en/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV \u003e BOOKS",
    "content": "Vol. 1 Nicolai Levashov. Spirit and Mind. Vol 1Using the theory of space heterogeneity the author continues to tear the veil of secrecy from the next riddles of nature. This time living nature and man is the focal point of cognition. The author formulates what the necessary and sufficient conditions are for the origin of life on planets. The simplicity and beauty of these concepts enables a reader, possibly for the first time in his or her life, to experience enlightening by knowledge, when there is a sense that knowledge becomes an inalienable part of self. In the first volume the author describes the nature and mechanism of human emotions and shows their role in the evolution of life in general and man in particular. He explains what love is in reality and this wonderful feeling does not lose its beauty because of this explanation, but on the contrary, allows man to understand what is going on with him and avoid unnecessary disappointments… Also, the author sheds light on the nature of memory and for the first time shows the mechanisms of short-term and long-term memory forming and on this basis reveals the mechanism of the origin of consciousness. The book contains 100 high-quality author’s illustrations.\n© Nicolai Levashov 1999, San Francisco.\nVol. 2 In the second volume of the book the author clearly shows the necessary and sufficient conditions for the appearance of consciousness at a certain level of development of life. The understanding of the mechanisms of memory and consciousness forming at the level of the material bodies of a spirit explains the phenomenon of life after death happening in the state of clinical death. Owing to it, these facts leave the category of inexplicable phenomena and pass to the natural phenomena of living nature. The phenomenon of reincarnation leaves the category of religious and mystic concepts and also passes to the category of real natural phenomena. The same happens with the concepts of “karma” and “sin” which stop being the instrument of manipulation of people’s consciousness by state and religious figures and turn into manifestations of natural laws. The understanding of all this makes man truly free and gives him the opportunity to be the creator of his own life, as only man himself, not God or anyone else, determines his deeds and bears full responsibility (not just the moral one) for them. © Nicolai Levashov 2003, San Francisco.\nVol. 3 Unwritten book..\nIn this volume the author continues to uncover to readers the secrets of nature. This time the nature of human psychical phenomena is the centre of his attention. The author shows a range of pioneering concepts concerning psychical phenomena of both man and society, which no one has ever touched upon before. He introduces new concepts, such as geo-psychology of man and the evolutional geo-psychology of society. These concepts allow seeing the development of earthly civilization and historical events of the past, present and even future from a quite different point of view. Instead of the “chaos” of events and the “tyranny” of individuals, which are favourite histori-ans’ subjects, we can see the pattern of events determined by real laws of nature which operate in human society. As a result, readers get the opportunity to understand the reasons for social events and phenomena and to see the puppeteers who have remained in the shadows for so long and went to great pains to declare anyone who suspected their presence but did not understand natural laws to be mad or cheaters. Also, the author introduces the concept of cosmic psychology and explains the influence of space phenomena on the development of our civilization. © Nicolai Levashov.\nDownload Only Volume 1 is available for download.\nDownload for free\n* Levashov-Spirit-and-Mind-Vol.1-EN.zip (1,5 МБ)\n* Spirit-and-Mind-Vol.1-Illustrations.zip (9,4 МБ)",
    "description": "Vol. 1 Nicolai Levashov. Spirit and Mind. Vol 1Using the theory of space heterogeneity the author continues to tear the veil of secrecy from the next riddles of nature. This time living nature and man is the focal point of cognition. The author formulates what the necessary and sufficient conditions are for the origin of life on planets. The simplicity and beauty of these concepts enables a reader, possibly for the first time in his or her life, to experience enlightening by knowledge, when there is a sense that knowledge becomes an inalienable part of self.",
    "tags": [
      "Levashov",
      "Books"
    ],
    "title": "\"Spirit and Mind\"",
    "uri": "/en/levashov/knigi/spirit-and-mind/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV \u003e BOOKS",
    "content": " Laws of nature are formed at the macrocosmic and microcosmic levels. Man, as a living creature, exists in a so-called intermediate world – between macro- and micro-world. In this intermediate world he has to deal only with manifestations of natural laws, but not with them as such. As a result, it is almost impossible to create a complete picture of the Universe. This occurs because man tends to use his sense organs to cognize nature. He fails, because human sense organs cannot give him such a possibility owing to the fact that they (organs) were created by nature only as a mechanism of adaptation to the ecological niche, which man occupies, not as a tool of cognition... © Nicolai Levashov 2002, San Francisco.",
    "description": "Laws of nature are formed at the macrocosmic and microcosmic levels. Man, as a living creature, exists in a so-called intermediate world – between macro- and micro-world. In this intermediate world he has to deal only with manifestations of natural laws, but not with them as such. As a result, it is almost impossible to create a complete picture of the Universe. This occurs because man tends to use his sense organs to cognize nature.",
    "tags": [
      "Levashov",
      "Books"
    ],
    "title": "\"The Anisotropic Universe\"",
    "uri": "/en/levashov/knigi/the-anisotropic-universe/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV \u003e BOOKS",
    "content": "Vol 1. Born in the USSR Nicolai Levashov. The mirror of my soul. Born in the USSR\"There are several reasons why I decided to write my own biography. First, whenever I had occasion to talk about some events of my life, my stories would often come back to me in the form of the most unimaginable \"folklore.\" In fact, my tales took on such \"facts\" and colorations that even I listened to them with interest. The second reason that impelled to such a \"feat\" was the fact that every now and then someone would appear and offer to write my biography—and every time something stopped me. Once I even agreed to have an American woman author garner my recollections onto audiocassettes and spent several days with her recording them. But then I changed my mind and gave up the offer. First of all, I had to expend a lot of time describing and explaining events that had happened to me. Secondly, to my utter astonishment, writers and journalists managed to distort everything despite their having my recorded recollections: this would include exaggerating, distorting facts and sometimes simply telling bare-faced lies...\" © Nicolai Levashov, 2006.\nVol 2. America the Real Thing \"Being born in the USSR and having lived in this country for thirty years before my departure to the USA, I was totally sure that the socialist regime was the creation of social parasites the purpose of which was to destroy the best part of the nation — the strong people as they were called in the Torah and the Old Testament — to break the back of the nation and to convert the remaining into slaves... Leaving the USSR, I was sure that at last I was able to break loose from the \"Empire of Evil\" into the free world — America! I thought and wanted to believe that beyond the iron curtain was real freedom!...anticipating a little, I would like to say that I found no freedom whatsoever in the USA! On the contrary, I saw another parasitic system where people were converted into slaves whilst considering themselves to be free...\" © Nicolai Levashov, 2008.\nDownload Download for free\n* Levashov-The-Mirror-of-My-Soul-Vol.1-EN.pdf (2,8 МБ)\n* Levashov-The-Mirror-of-My-Soul-Vol.2-EN.doc (2,3 МБ)",
    "description": "Vol 1. Born in the USSR Nicolai Levashov. The mirror of my soul. Born in the USSR\"There are several reasons why I decided to write my own biography. First, whenever I had occasion to talk about some events of my life, my stories would often come back to me in the form of the most unimaginable \"folklore.\" In fact, my tales took on such \"facts\" and colorations that even I listened to them with interest.",
    "tags": [
      "Levashov",
      "Books"
    ],
    "title": "\"The mirror of my soul\"",
    "uri": "/en/levashov/knigi/the-mirror-of-my-soul/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV \u003e BOOKS",
    "content": " \"As any Russian I always has been interested in the history of my country. In my childhood I red a lot of historical novels, books on history of both Russia and other countries of the world. As I went on gaining and analyzing historical information available, my heart and mind began to fill with bewilderment and indignation. Every nation on Earth, independently their real role in the destiny of the world, wrote their own Great History using for this purpose both real events, folk legends and some times purely fictitious events. There would be nothing unusual in it but except for just one snag … everything applied to the history of Russia was filled with uncovered hatred of those who wrote its history. According to their opinion the Slavs lived in earthen pits up to the 9th century and were such primitive that even did not have their own state system and had to invite Varangians to govern. Also they lived in horrified ignorance until in the 10th century two saints, Cyril and Mefodiy, created the Slavonic written language on the basis of the Greek language shedding thus the \"light of knowledge\" on the obtuse Slavs. Also Mongols had kept Russian people in slavery for three hundred years and only when Peter the Great cut through a \"window\" to Europe and transformed Russia in accordance with European standards, Russia became the Great Empire, etc. Any well-educated person understands perfectly that history is written according to the orders of those in power and is rewritten following their requirements and desire. Therefore, it would be appropriate to ask who were these \"historians\" and why were they reluctant to create for Russian rulers something similar to that, what had been created for Jewish, Chinese, Greek, Roman and other nations and empires?..\" The book contains 42 high-quality author's illustration. © Nicolai Levashov, 2007.\nDownload This book is still banned from distribution in Russia..",
    "description": "\"As any Russian I always has been interested in the history of my country. In my childhood I red a lot of historical novels, books on history of both Russia and other countries of the world. As I went on gaining and analyzing historical information available, my heart and mind began to fill with bewilderment and indignation. Every nation on Earth, independently their real role in the destiny of the world, wrote their own Great History using for this purpose both real events, folk legends and some times purely fictitious events.",
    "tags": [
      "Levashov",
      "Books"
    ],
    "title": "\"Russian History Viewed through Distorted Mirrors\"",
    "uri": "/en/levashov/knigi/russian-in-a-distorted-mirrors/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV \u003e BOOKS",
    "content": " An amazing book written by the wife of Nikolai Viktorovich fully reflects and complements everything he said and wrote about. “Svetlana” means “Bearing Light”. It very rarely happens that the destiny of a person, his deeds and his name fully coincide, as happened in Svetlana de Rohan-Levashov’s case. All her life, from earliest childhood, was permeated with an aspiration to Light, Knowledge and spiritual development. That her fate is unusual is the very least one can say. From the very beginning of her life she had to adapt to the fact that she differed greatly from people around her, that she could do a lot of things which were inaccessible and incomprehensible for others. Being yet very little, Svetlana had to study and master her abilities, to learn how to control and use them correctly. She early experienced the bitter taste of misunderstanding and mistrust, envy and cruelty, loneliness and hatred. Her miraculous innate abilities were misunderstood and unclaimed; she had to survive and live in this world, a very dangerous and insidious world, especially, for a lonely little girl…\n© Svetlana de Rohan-Levashov, 2010\nDownload Download for free\n* Levashova-Revelation-EN.pdf (2,1 МБ) ",
    "description": "An amazing book written by the wife of Nikolai Viktorovich fully reflects and complements everything he said and wrote about. “Svetlana” means “Bearing Light”. It very rarely happens that the destiny of a person, his deeds and his name fully coincide, as happened in Svetlana de Rohan-Levashov’s case. All her life, from earliest childhood, was permeated with an aspiration to Light, Knowledge and spiritual development. That her fate is unusual is the very least one can say.",
    "tags": [
      "Levashova",
      "Books"
    ],
    "title": "\"Revelation\"",
    "uri": "/en/levashov/knigi/revelation/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e LEVASHOV \u003e BOOKS",
    "content": " Modern medicine went astray and, losing “Ariadne's thread”, is unable to get out of the labyrinth which it has created. In the middle of the 20th century physicians said that, when they had the most precise devices for diagnostics and the necessary medications, they could bring humanity to a golden era of universal health... They’ve got all this... But, nevertheless, people fall ill even more than they did before. Children are born with already damaged immune systems; on visiting a hospital or a clinic, a relatively healthy person runs a great risk of leaving it having a number of diseases, which pretty often end in death, by simply inhaling the air of these “temples of health”. In this book the author explains the reasons for this and describes the medicine of the future which is already working and demonstrates real results which confirm the rightness of this new way. In this book the author explains how the living organism works and how and why illnesses and pathologies appear. He describes the mechanisms of the scanning of the organism, the methods for determining diseases’ original causes, the strategy and tactic of healing and restoration of the organism to a healthy state, including its genetic correction. © Nicolai Levashov.\nUnwritten book",
    "description": "Modern medicine went astray and, losing “Ariadne's thread”, is unable to get out of the labyrinth which it has created. In the middle of the 20th century physicians said that, when they had the most precise devices for diagnostics and the necessary medications, they could bring humanity to a golden era of universal health... They’ve got all this... But, nevertheless, people fall ill even more than they did before. Children are born with already damaged immune systems; on visiting a hospital or a clinic, a relatively healthy person runs a great risk of leaving it having a number of diseases, which pretty often end in death, by simply inhaling the air of these “temples of health”.",
    "tags": [
      "Levashov",
      "Books"
    ],
    "title": "\"Laws of Healing\"",
    "uri": "/en/levashov/knigi/laws-of-healing/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e Tags",
    "content": "",
    "description": "",
    "tags": [],
    "title": "Tag : Books",
    "uri": "/en/tags/books/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK",
    "content": "",
    "description": "",
    "tags": [],
    "title": "Categories",
    "uri": "/en/categories/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e Tags",
    "content": "",
    "description": "",
    "tags": [],
    "title": "Tag : Levashov",
    "uri": "/en/tags/levashov/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK \u003e Tags",
    "content": "",
    "description": "",
    "tags": [],
    "title": "Tag : Levashova",
    "uri": "/en/tags/levashova/index.html"
  },
  {
    "breadcrumb": "RAVESTNIK",
    "content": "",
    "description": "",
    "tags": [],
    "title": "Tags",
    "uri": "/en/tags/index.html"
  }
]
