const player = new Plyr('#player', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player.buffered = 0;

const player1 = new Plyr('#player-1', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player1.buffered = 0;

const player2 = new Plyr('#player-2', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player2.buffered = 0;

const player3 = new Plyr('#player-3', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player3.buffered = 0;

const player4 = new Plyr('#player-4', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player4.buffered = 0;

const player5 = new Plyr('#player-5', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player5.buffered = 0;

const player6 = new Plyr('#player-6', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player6.buffered = 0;

const player7 = new Plyr('#player-7', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player7.buffered = 0;

const player8 = new Plyr('#player-8', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player8.buffered = 0;

const player9 = new Plyr('#player-9', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player9.buffered = 0;

const player10 = new Plyr('#player-10', {
    quality: { default: 720 },
	blankVideo: '/scripts/plyr/blank.mp4',
	iconUrl: '/scripts/plyr/plyr.svg',
	invertTime: false,
	captions: { active: true, language: 'auto', update: false },
	speed: { selected: 1, options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2] },
});
player10.buffered = 0;

$.each(players,function(i){
players[i].on('play', event => {
for (let a = 0; a < players.length; a++){
if (a!=i) {
players[a].pause();
}
}
});
});